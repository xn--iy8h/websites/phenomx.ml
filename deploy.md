# deployment

The site is deployed to <https://phenomx.ch/> ([infomaniak][ik]) via ftp.

```sh
sh upload.sh
```

(The site <https://phenomx.ml/> is deploy via netlify.app (user:michel47))

[ik]:https://manager.infomaniak.com/ftp/index.php?sServer=phenomx.ch


```
dig www.phenomx.ch
# 128.65.195.180
```
