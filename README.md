---
layout: default
---
## PhonomX® website build (technical details corner)

* Status / Uptime : [uptimerobot](https://stats.uptimerobot.com/EW7xrSWLD8/787680776)
* repository: [git:/xn--iy8h/websites/phenomx.ml][1]
* pipeline: [![GitLab Status](https://gitlab.com/xn--iy8h/websites/phenomx.ml/badges/master/pipeline.svg)](https://gitlab.com/xn--iy8h/websites/phenomx.ml/-/pipelines)
* deploy: [![Netlify Status](https://api.netlify.com/api/v1/badges/6638d742-cc6b-476e-8d85-97d5e8e00d5a/deploy-status)](https://app.netlify.com/sites/phenomx/deploys)

* back to the [questionnaire](./#questionnaire)




our website is deployed at <https://www.phenomx.ml/>,
or alternatively at
[<script>document.write(location.href);</script>](index.html)

<br>and can be preview at <https://🕸.gitlab.io/websites/phenomx.ml>.

<br>The IPFS version is <https://ipfs.blockring™.ml/ipfs/{{site.data.bot.key}}>,
or <https://ipfs.phenomix.ml/ipfs/{{site.data.bot.key}}>



~[Dr I·T](https://www.drit.ml/)

[1]: https://gitlab.com/xn--iy8h/websites/phenomx.ml

