---
layout: default
---
## About PhenomX

- Introduction : [Hormonal health rhythm rejuvenation][4]
- Who is Doctor Draper : [Bio][5]
- Mass Challenge [Application][1]
- Mass Challenge [Pitch Deck][9] ([ppt][2])
- PhenomX [Questionnaire](./#questionnaire)

## what is PhenomX ?

{% include pitch.htm %}

---
### System Administration Courtesy Dr I·T

- Page [technical details][8]
- Git Repository: [GitLab][6]
- Who is Doctor I·T ? read more at [https://drit.ml][10]


[1]: https://ipfs.blockring™.ml/ipfs/QmPynXQbF1TcZXHBx1Fik7ZHTPRhdGmn4KM8H1moAawHYG/PhenomX%20-%20Precision%20Nutrition%20for%20Women%20-%20MassChallenge.html
[2]: https://ipfs.blockring™.ml/ipfs/QmYSCSXXGdiUgudu5BbfMxrSnWqnpkgzn8itk5URkkM4v3/PhenomX_Tech4Eva_29-March-2021_v0.02-najyd.pptx
[3]: https://bl.ocks.org/jinroh/7524988
[4]: https://gitlab.com/xn--iy8h/websites/phenomx.ml/-/blob/master/_includes/intro.md
[5]: https://nutrauhealth.com/bios.html#colleen
[6]: https://gitlab.com/xn--iy8h/websites/phenomx.ml
[7]: https://gitlab.com/xn--iy8h/websites/phenomx.ml/-/commits/master
[8]: README.html
[9]: https://1drv.ms/p/s!AiFEzSvkv5sGjF0FFahdDIbJQXy0?e=GgI4NL
[10]: https://www.drit.ml/about/


<!-- [commit history][7] -->
  


